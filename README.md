# Flectra Community / muk_misc

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[muk_hr_security](muk_hr_security/) | 1.0.1.0.1| Security Features
[muk_account_accountant](muk_account_accountant/) | 1.0.1.0.0| Financial and Analytic Accounting
[muk_hr_utils](muk_hr_utils/) | 1.0.1.0.4| Utility Features
[muk_account_bank_statement_import_sheet](muk_account_bank_statement_import_sheet/) | 1.0.2.0.0| Bank Statement Import Wizard


